<?php

namespace App\Tests;

use App\Service\MarkdownConverter\MarkdownConverterImpl;
use PHPUnit\Framework\TestCase;

class MarkdownConverterTest extends TestCase
{
    public function testNormalText(): void
    {
        $text = "test";
        $markDownConverter = new MarkdownConverterImpl();
        $res = $markDownConverter->toHtml($text);
        $this->assertEquals($text,$res);
    }

    public function testItalic(): void
    {
        $text1 = "*test*";
        $text2 = "_test_";
        $markDownConverter = new MarkdownConverterImpl();
        $res = $markDownConverter->toHtml($text1);

        $this->assertStringContainsString("<em>",$res);
        $res = $markDownConverter->toHtml($text2);
        $this->assertStringContainsString("<em>",$res);

    }

    public function testStrong(): void
    {
        $text1 = "**test**";
        $text2 = "__test__";
        $markDownConverter = new MarkdownConverterImpl();
        $res = $markDownConverter->toHtml($text1);

        $this->assertStringContainsString("<strong>",$res);
        $res = $markDownConverter->toHtml($text2);
        $this->assertStringContainsString("<strong>",$res);

    }

    public function testStrongAndItalic(): void
    {
        $text1 = "***test***";
        $text2 = "___test___";
        $markDownConverter = new MarkdownConverterImpl();
        $res = $markDownConverter->toHtml($text1);

        $this->assertStringContainsString("<strong><em>",$res);
        $res = $markDownConverter->toHtml($text2);
        $this->assertStringContainsString("<strong><em>",$res);

    }
}
