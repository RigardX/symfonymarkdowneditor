<?php

namespace App\Service\MarkdownConverter;


class MarkdownConverterImpl implements MarkdownConverter
{
    const REGEX_STRONG = '/(\*\*|__)(.*?)\1/';
    const REGEX_EM = '/(\*|_)(.*?)\1/';

    /**
     * метод реализованный лично мною без библиотеки
     * возвращает html преобразованный из markdown,учитывает только жирность и курсив
     * @param string $text
     * @return string
     */
    public function toHtml(string $text): string
    {
        $text = $this->convertStrong($text);
        return $this->convertEm($text);
    }

    private function convertStrong(string $text): string{
        return preg_replace(self::REGEX_STRONG,'<strong>$2</strong>',$text);
    }

    private function convertEm(string $text): string{

        while(preg_match(self::REGEX_EM,$text) === 1){
            $text =  preg_replace(self::REGEX_EM,'<em>$2</em>',$text);
        }
        return $text;
    }
}