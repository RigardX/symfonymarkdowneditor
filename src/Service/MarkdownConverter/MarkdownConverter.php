<?php

namespace App\Service\MarkdownConverter;

interface MarkdownConverter
{
    public function toHtml(string $text): string;
}