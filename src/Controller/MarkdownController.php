<?php

namespace App\Controller;

use App\Service\MarkdownConverter\MarkdownConverter;
use App\Service\MarkdownConverter\MarkdownConverterImpl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MarkdownController extends AbstractController
{
    private $markdownConverter;

    public function __construct(MarkdownConverter $markdownConverter)
    {
        $this->markdownConverter = $markdownConverter;
    }

    public function index() : Response{
        return $this->render('base.html.twig');
    }

    public function convert(Request $request) : JsonResponse{
        $data = $request->toArray();
        $html = $this->markdownConverter
            ->toHtml($data["text"]);
        return new JsonResponse([
            "html" => $html
        ]);
    }
}